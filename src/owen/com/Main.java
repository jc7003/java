package owen.com;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	private static ArrayList<MyVideo> mMyVideoList;
	private static final String[] VIDEO_NAME = {"MIB", "dead pool"};
	private static final int[] VIDEO_PRICE = {100, 200};
	
	public static void main(String[] args) {
		//資料初始化
		initData();
		//監聽 console 輸入值
		Scanner s = new Scanner(System.in);
		while(true){
			String str = s.nextLine();
			MyVideo video = searchVideoByName(str);
			System.out.println(video.getName() +" price:" + video.getPrice());
		}
	}
	
	private static void initData(){
		mMyVideoList = new ArrayList<MyVideo>();
		
		for(int i = 0; i < VIDEO_NAME.length; i++){
			MyVideo mvideo = new MyVideo(i, VIDEO_NAME[i], VIDEO_PRICE[i]);
			mMyVideoList.add(mvideo);
		}
	}
	
	private static MyVideo searchVideoByName(String name){
		for(MyVideo video : mMyVideoList){
			if(video.getName().contains(name)){
				return video;
			}
		}
		
		return null;
	}
}
