package owen.com;

public class MyVideo {

	private int mID;
	private String mName;
	private int mPrice;
	
	public MyVideo(int id, String name, int price){
		this.mID = id;
		this.mName = name;
		this.mPrice = price;
	}
	
	public String getName(){
		return mName;
	}
	
	public int getID(){
		return mID;
	}
	
	public int getPrice(){
		return mPrice;
	}
}
